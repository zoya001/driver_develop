#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>
#include <QMessageBox>
#include <QDebug>
#include <system_error>
#include <string>
#include "windows.h"
#include "fltuser.h"

using namespace std;

class Backend : public QObject
{
    Q_OBJECT
public:
    explicit Backend(QObject *parent = nullptr);
    void EnablePrivilege();
    BOOL SetPrivilege(HANDLE hToken, LPCTSTR lpszPrivilege, BOOL bEnablePrivilege);

    HRESULT hResult;

    void load();
    void unload();
};

#endif // BACKEND_H
