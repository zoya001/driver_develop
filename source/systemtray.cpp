#include "systemtray.h"
#include <QMenu>
#include <QSystemTrayIcon>


SystemTray::SystemTray(QObject *parent) : QObject(parent)
{
    // Создаём контекстное меню
    QMenu *trayIconMenu = new QMenu();

    trayIconMenu->addAction("Загрузить драйвер", this, SLOT(slotLoad()));
    trayIconMenu->addAction("Выгрузить драйвер", this, SLOT(slotUnload()));
    trayIconMenu->addAction("Выход", this, SLOT(slotExit()));

    /* Инициализируем иконку трея, устанавливаем иконку,
     * а также задаем всплывающую подсказку
     * */
    trayIcon = new QSystemTrayIcon();
    trayIcon->setContextMenu(trayIconMenu);
    trayIcon->setIcon(QIcon(":/success-green-check-mark.png"));
    trayIcon->show();
    trayIcon->setToolTip("PassThrough");
}


void SystemTray::slotLoad()
{
    backend.load();
}

void SystemTray::slotUnload()
{
    backend.unload();
}

void SystemTray::slotExit()
{
    QApplication::quit();
    trayIcon->hide();
}
