#ifndef SYSTEMTRAY_H
#define SYSTEMTRAY_H

#include <QObject>
#include <QAction>
#include <QSystemTrayIcon>
#include <QDebug>
#include "backend.h"
#include <QApplication>

class SystemTray : public QObject
{
    Q_OBJECT
public:
    explicit SystemTray(QObject *parent = 0);

signals:
    void signalIconActivated();

private slots:
    void slotLoad();
    void slotUnload();
    void slotExit();

private:
    /* Объявляем объект будущей иконки приложения для трея */
    QSystemTrayIcon * trayIcon;
    Backend backend;
};

#endif // SYSTEMTRAY_H
