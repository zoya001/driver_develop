#include "backend.h"

Backend::Backend(QObject *parent) : QObject(parent)
{
    EnablePrivilege();
}

void Backend::load()
{
    hResult = FilterLoad(L"passThrough");

    if (!IS_ERROR(hResult)) QMessageBox::information(nullptr, "Успех", "Драйвер успешно загружен");

    else {
        string error = system_category().message(hResult);
        QString errorQString = QString::fromLocal8Bit(error.c_str());
        QMessageBox::critical(nullptr, "Внимание", "Обнаружена ошибка: " + errorQString);
    }
}

void Backend::unload()
{
    hResult = FilterUnload(L"passThrough");

    if (!IS_ERROR(hResult)) QMessageBox::information(nullptr, "Успех", "Драйвер успешно выгружен");

    else {
        string error = system_category().message(hResult);
        QString errorQString = QString::fromLocal8Bit(error.c_str());
        QMessageBox::critical(nullptr, "Внимание", "Обнаружена ошибка: " + errorQString);
    }
}

void Backend::EnablePrivilege()
{
    HANDLE hToken;
    HANDLE hCurrentProcess;
    hCurrentProcess = GetCurrentProcess();
    OpenProcessToken(hCurrentProcess, TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken);
    SetPrivilege(hToken, SE_LOAD_DRIVER_NAME, TRUE);
}

BOOL Backend::SetPrivilege(
        HANDLE hToken,          // access token handle
        LPCTSTR lpszPrivilege,  // name of privilege to enable/disable
        BOOL bEnablePrivilege)   // to enable or disable privilege

{
    TOKEN_PRIVILEGES tp;
    LUID luid;

    if ( !LookupPrivilegeValue(
             NULL,            // lookup privilege on local system
             lpszPrivilege,   // privilege to lookup
             &luid ) )        // receives LUID of privilege
    {
        return FALSE;
    }

    tp.PrivilegeCount = 1;
    tp.Privileges[0].Luid = luid;
    if (bEnablePrivilege)
        tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
    else
        tp.Privileges[0].Attributes = 0;

    // Enable the privilege or disable all privileges.

    if ( !AdjustTokenPrivileges(
             hToken,
             FALSE,
             &tp,
             sizeof(TOKEN_PRIVILEGES),
             (PTOKEN_PRIVILEGES) NULL,
             (PDWORD) NULL) )
    {
        return FALSE;
    }

    if (GetLastError() == ERROR_NOT_ALL_ASSIGNED)

    {
        return FALSE;
    }

    return TRUE;
}




